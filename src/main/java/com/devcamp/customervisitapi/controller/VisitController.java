package com.devcamp.customervisitapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customervisitapi.model.Visit;
import com.devcamp.customervisitapi.service.VisitService;

@RestController
public class VisitController {
    @Autowired
    VisitService visitService;
    @GetMapping("/visits")
    public ArrayList<Visit> getVisits(){
        return visitService.getAllVisits();
    }

}
