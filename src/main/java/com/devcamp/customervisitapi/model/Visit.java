package com.devcamp.customervisitapi.model;

import java.util.Date;

public class Visit extends Customer{
    private Customer customer ;
    private Date date ;
    private double serviceExpense ;
    private double productExpense ;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getServiceExpense() {
        return serviceExpense;
    }

    public void setServiceExpense(double serviceExpense) {
        this.serviceExpense = serviceExpense;
    }

    public double getProductExpense() {
        return productExpense;
    }

    public void setProductExpense(double productExpense) {
        this.productExpense = productExpense;
    }

    public double getTotalExpense() {

        return 0 ;
    }


    public Visit(String name , Date date) {
        super(name);
        this.date = date;
    }

    @Override
    public String toString() {
        return "Visit [customer=" + customer + ", date=" + date + ", productExpensive=" + productExpense
                + ", serviceExpensive=" + serviceExpense + "]";
    }

}
