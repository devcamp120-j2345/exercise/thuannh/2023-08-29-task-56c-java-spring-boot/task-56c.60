package com.devcamp.customervisitapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.customervisitapi.model.Customer;

@Service
public class CustomerService {
    Customer customer1 = new Customer("Thuan");
    Customer customer2 = new Customer("Toan");
    Customer customer3 = new Customer("Hung");
    public ArrayList<Customer> getAllCustomers(){
        ArrayList<Customer> customerList = new ArrayList<>();
        customerList.add(customer1);
        customerList.add(customer2);
        customerList.add(customer3);
        return customerList; 
    }

}