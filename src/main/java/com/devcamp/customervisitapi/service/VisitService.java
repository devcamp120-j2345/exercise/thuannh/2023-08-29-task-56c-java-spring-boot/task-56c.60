package com.devcamp.customervisitapi.service;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.customervisitapi.model.Visit;

@Service
public class VisitService extends CustomerService{
    Visit visit1 = new Visit(customer1.getName(), new Date());
    Visit visit2 = new Visit(customer2.getName(), new Date());
    Visit visit3 = new Visit(customer3.getName(), new Date());
    public ArrayList<Visit> getAllVisits(){
        ArrayList<Visit> visitList = new ArrayList<>();
        visitList.add(visit1);
        visitList.add(visit2);
        visitList.add(visit3);
        return visitList;

    }

}
